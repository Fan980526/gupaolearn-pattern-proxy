package com.fan.gupaolearn.pattern.proxy;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        System.out.println("Hello World!");

        GpMeipo gpMeipo = new GpMeipo();
        IPerson zhangsan = gpMeipo.getInstance(new Zhangsan());
//        zhangsan.findLove();
//        zhangsan.buyInsure();
        zhangsan.findWork("年薪10万");
    }
}
