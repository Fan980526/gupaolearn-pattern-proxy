package com.fan.gupaolearn.pattern.proxy;


public class Zhangsan implements IPerson {

    @Override
    public void findLove() {
        System.out.println("张三要求：肤白貌美大长腿");
    }

    @Override
    public void buyInsure() {
        System.out.println("30万");
    }

    @Override
    public void findWork(String salary) {
        System.out.println("找工作，薪资要求：" + salary);
    }

}
