package com.fan.gupaolearn.pattern.proxy.gpproxy;

import java.lang.reflect.Method;


public interface GPInvocationHandler {

    Object invoke(Object proxy, Method method, Object[] args) throws Throwable;

}
