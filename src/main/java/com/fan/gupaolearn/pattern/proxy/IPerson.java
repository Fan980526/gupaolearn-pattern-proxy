package com.fan.gupaolearn.pattern.proxy;

/**
 * Created by Tom.
 */
public interface IPerson {

    void findLove();

    void buyInsure();

    void findWork(String salary);

}
